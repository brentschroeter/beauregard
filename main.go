package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"github.com/BurntSushi/toml"
	"github.com/dghubble/go-twitter/twitter"
	"github.com/dghubble/oauth1"
)

type creds struct {
	SlackWebhook          string `toml:"slack_webhook"`
	TwitterAccessSecret   string `toml:"twitter_access_secret"`
	TwitterAccessToken    string `toml:"twitter_access_token"`
	TwitterConsumerKey    string `toml:"twitter_consumer_key"`
	TwitterConsumerSecret string `toml:"twitter_consumer_secret"`
}

type subscription struct {
	Handle   string   `toml:"handle"`
	Keywords []string `toml:"keywords"`
}

type config struct {
	Creds         creds          `toml:"creds"`
	Subscriptions []subscription `toml:"subscriptions"`
}

// newConfig parses the TOML-format configuration in the file located at
// the given path.
func newConfig(path string) (*config, error) {
	var cfg config
	dat, err := ioutil.ReadFile(path)
	if err != nil {
		return &cfg, err
	}
	_, err = toml.Decode(string(dat), &cfg)
	return &cfg, err
}

// initTwitter constructs a new Twitter client based on the API
// credentials in the configuration.
func initTwitter(cfg *config) *twitter.Client {
	// We're going to be streaming tweets, so OAuth1 (not OAuth2) is required.
	config := oauth1.NewConfig(cfg.Creds.TwitterConsumerKey, cfg.Creds.TwitterConsumerSecret)
	token := oauth1.NewToken(cfg.Creds.TwitterAccessToken, cfg.Creds.TwitterAccessSecret)

	// Example code uses oauth1.NoContext, which is just context.TODO()
	// underneath. However, this seems misleading because the intent
	// (at least in this case) fits the bill for context.Background()
	// and thus is neither ambiguous nor unimplemented.
	httpClient := config.Client(context.Background(), token)

	return twitter.NewClient(httpClient)
}

// lookupAllUsers retrieves account information (including, notably, the
// user ID) for each of the Twitter handles referenced in the
// configuration.
func lookupAllUsers(cfg *config, t *twitter.Client) ([]twitter.User, error) {
	// jerry-rigged set type for collecting an array of unique handles
	set := make(map[string]struct{})
	var member struct{}
	for _, sub := range cfg.Subscriptions {
		set[sub.Handle] = member
	}
	handles := make([]string, 0, len(set))
	for h := range set {
		handles = append(handles, strings.ReplaceAll(h, "@", ""))
	}

	// Lookups returning more than 100 users require multiple requests.
	const batchSize = 100
	users := make([]twitter.User, 0, len(handles))
	for n := 0; n < len(handles); n += batchSize {
		end := n + batchSize
		if end > len(handles) {
			end = len(handles)
		}
		// TODO Twitter recommends POST requests for extensive lookup
		// parameters, but go-twitter always uses GET requests.
		p := twitter.UserLookupParams{ScreenName: handles[n:end]}
		u, _, err := t.Users.Lookup(&p)
		if err != nil {
			return users, err
		}
		users = append(users, u...)
	}
	return users, nil
}

// tweetMatches returns true if the tweet matches the rules for any of
// the Beauregard-style subscriptions.
func tweetMatches(subs []subscription, tweet *twitter.Tweet) bool {
	text := strings.ToLower(tweet.Text)
	for _, sub := range subs {
		h := strings.ToLower(strings.ReplaceAll(sub.Handle, "@", ""))
		if h != strings.ToLower(tweet.User.ScreenName) {
			continue
		}
		match := true
		for _, kw := range sub.Keywords {
			if !strings.Contains(text, strings.ToLower(kw)) {
				match = false
				break
			}
		}
		if match {
			return true
		}
	}
	return false
}

// postToSlack invokes a Slack incoming webhook to post the details of
// the tweet to the default Slack channel.
func postToSlack(url string, tweet *twitter.Tweet) error {
	type slackMessage struct {
		Text string `json:"text"`
	}

	text := tweet.Text
	if tweet.Truncated {
		text = tweet.ExtendedTweet.FullText
	}

	link := fmt.Sprintf("https://twitter.com/%s/status/%s", tweet.User.ScreenName, tweet.IDStr)
	slackBody := fmt.Sprintf("@%s:\n```%s```\n%s", tweet.User.ScreenName, text, link)
	msg, err := json.Marshal(slackMessage{Text: slackBody})
	if err != nil {
		return err
	}
	_, err = http.Post(url, "application/json", bytes.NewBuffer(msg))
	return err
}

func main() {
	if len(os.Args) != 2 {
		fmt.Println("Usage: beauregard <TOML config>")
		os.Exit(1)
	}

	cfg, err := newConfig(os.Args[1])
	if err != nil {
		panic(err)
	}

	t := initTwitter(cfg)

	users, err := lookupAllUsers(cfg, t)
	if err != nil {
		panic(err)
	}
	uids := make([]string, len(users))
	for i, v := range users {
		uids[i] = v.IDStr
	}

	demux := twitter.NewSwitchDemux()
	demux.Tweet = func(tweet *twitter.Tweet) {
		match := tweetMatches(cfg.Subscriptions, tweet)
		// Uncomment for debugging print-outs.
		//		fmt.Printf("%s: %s [%s]\n", tweet.User.ScreenName, tweet.Text,
		//			strconv.FormatBool(match))
		if match {
			if err = postToSlack(cfg.Creds.SlackWebhook, tweet); err != nil {
				// "Don't Panic."
				fmt.Fprintln(os.Stderr, err)
			}
		}
	}
	p := twitter.StreamFilterParams{Follow: uids}
	stream, err := t.Streams.Filter(&p)
	if err != nil {
		panic(err)
	}
	go demux.HandleChan(stream.Messages)

	// Nice to have since misspelled handles are silently ignored.
	fmt.Println("Streaming tweets from:")
	for _, u := range users {
		fmt.Println(u.ScreenName)
	}

	// Stop the stream and exit upon receiving SIGINT or SIGTERM.
	ch := make(chan os.Signal)
	signal.Notify(ch, syscall.SIGINT, syscall.SIGTERM)
	<-ch
	fmt.Println("Stopping stream...")
	stream.Stop()
}
