FROM golang:1.12-alpine AS builder

# Install some dependencies needed to build the project
RUN apk add bash ca-certificates git gcc g++ libc-dev
 
# Force the go compiler to use modules
ENV GO111MODULE=on

WORKDIR /go/src/app

COPY ./go.mod .
COPY ./go.sum .

RUN go mod download

COPY ./main.go .

# Build the binary
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -o /go/bin/beauregard

FROM golang:1.12-alpine

COPY --from=builder /go/bin/beauregard /go/bin/beauregard

ENTRYPOINT ["/go/bin/beauregard"]
