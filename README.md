# Beauregard at a Glance

Tweeting is hard. Beauregard helps to filter the signal from the noise, keeping you abreast of the lives and topics you care about without spending time consuming content you don't.

# Configuration

The executable takes one argument, the path to a TOML configuration file. See below for an example config.

```toml
[creds]

twitter_consumer_key = "my_twitter_consumer_key"
twitter_consumer_secret = "my_twitter_consumer_secret"
twitter_access_token = "my_twitter_access_token"
twitter_access_secret = "my_twitter_access_secret"
# Slack is a simple, inexpensive, and familiar way to push notifications to various devices.
slack_webhook = "https://my.slack.webhook/"

[[subscriptions]]

handle = "@someone_to_follow"
keywords = [ "some words", "#hashtag" ] # all must match for subscription to match

[[subscriptions]]

handle = "@someone_else_to_follow"
# "keywords" is optional
```
